Name:           glassfish-servlet-api
Version:        4.0.1
Release:        2%{?dist}
Summary:        Java Servlet API
License:        (CDDL-1.0 or GPLv2 with exceptions) and ASL 2.0
URL:            https://javaee.github.io/servlet-spec/

Source0:        https://github.com/javaee/servlet-spec/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:        http://www.apache.org/licenses/LICENSE-2.0.txt

BuildArch:      noarch

BuildRequires:  java-devel >= 1:1.6.0
BuildRequires:  jvnet-parent
BuildRequires:  maven-local
BuildRequires:  maven-plugin-bundle
BuildRequires:  maven-source-plugin


%description
The javax.servlet package contains a number of classes 
and interfaces that describe and define the contracts between 
a servlet class and the runtime environment provided for 
an instance of such a class by a conforming servlet container.

%package javadoc
Summary:        Javadoc for %{name}

%description javadoc
API documentation for %{name}.

%prep
%setup -q -n servlet-spec-%{version}

%pom_remove_plugin :maven-remote-resources-plugin
%pom_remove_plugin :maven-javadoc-plugin
%pom_remove_plugin :glassfish-copyright-maven-plugin

cp -p %{SOURCE1} .
# README contains also part of javax.servlet-api license
cp -p src/main/resources/META-INF/README .
%mvn_file :javax.servlet-api glassfish-servlet-api

%build
%mvn_alias : javax.servlet:servlet-api
%mvn_alias : org.apache.geronimo.specs:geronimo-servlet_3.0_spec
%mvn_alias : org.eclipse.jetty.orbit:javax.servlet
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc README
%license LICENSE-2.0.txt LICENSE

%files javadoc -f .mfiles-javadoc
%doc README
%license LICENSE-2.0.txt LICENSE

%changelog
* Tue Nov 05 2019 Mikolaj Izdebski <mizdebsk@redhat.com> - 4.0.1-2
- Mass rebuild for javapackages-tools 201902

* Thu Jul 18 2019 Marian Koncek <mkoncek@redhat.com> - 4.0.1-1
- Update to upstream version 4.0.1

* Fri May 24 2019 Mikolaj Izdebski <mizdebsk@redhat.com> - 3.1.0-15
- Mass rebuild for javapackages-tools 201901

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.0-14
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.0-13
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.0-12
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Tue Jun 21 2016 gil cattaneo <puntogil@libero.it> 3.1.0-11
- add missing build requires

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 3.1.0-10
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1.0-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Jun 3 2015 Alexander Kurtakov <akurtako@redhat.com> 3.1.0-8
- Add alias for org.eclipse.jetty.orbit:javax.servlet.
- Fix javadoc compilation.

* Fri Feb 13 2015 gil cattaneo <puntogil@libero.it> 3.1.0-7
- introduce license macro

* Mon Aug  4 2014 Mikolaj Izdebski <mizdebsk@redhat.com> - 3.1.0-6
- Add alias for Geronimo servlet API

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1.0-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Jun 5 2014 Alexander Kurtakov <akurtako@redhat.com> 3.1.0-4
- Add javax.servlet:servlet-api alias.

* Fri Mar 28 2014 Michael Simacek <msimacek@redhat.com> - 3.1.0-3
- Use Requires: java-headless rebuild (#1067528)

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 3.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Thu Jul 04 2013 gil cattaneo <puntogil@libero.it> - 3.1.0-1
- Update to 3.1.0

* Sat Mar 09 2013 David Xie <david.scriptfan@gmail.com> - 3.1-0.1.b07
- Initial version of package
